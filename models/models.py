# -*- coding: utf-8 -*-

from odoo import models, fields, api

class informe(models.Model):
       _name = 'informe.informe'

       product_id = fields.Many2one('product.product')
       partner_id = fields.Many2one('res.partner')
       cuerpo_informe = fields.Char(string="Contenido del Informe")
       img1 =  fields.Binary(string="Imagen #1")
       img2 =  fields.Binary(string="Imagen #2")
       img3 =  fields.Binary(string="Imagen #3")
       img4 =  fields.Binary(string="Imagen #4")

#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100
